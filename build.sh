#!/bin/sh
# Requires pandoc, texlive and eisvogel latex template https://github.com/Wandmalfarbe/pandoc-latex-template
pandoc report.md metadata.yaml -o report.pdf --from markdown --template eisvogel --listings
