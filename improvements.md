
# Improvements to Dark Crystal Protocol and Briar features

## Improvements to Briar features following [user testing report](https://darkcrystal.pw/assets/briar-case-project-report.pdf)

### Social backup

#### Back end

- [x] Fix bug with deleting contacts with whom we have sent/received social backup messages
- [ ] Fix Transport key exchange after recovery - ***Work in progress***
- [x] Failure to recover on mismatched shards (shards from different sets) - ***Basic fix - could be improved***
- [x] Add persistent storage of returned shards
- [x] Fix setting default slider position on choosing threshold
- [x] Improve error handling during shard return
- [x] Improve handling of invalid/corrupt shard messages
- Shard return over Bluetooth - ***Out of scope for this development round***
- Remote shard return - ***Not possible in the next development round due to time constraints***

#### UI

- [x] Change 'Got it' button used on confirmation screens 
- [x] Change wording on shard sent confirmations (remove word shard) 
- [x] Disable 'help recover account' menu option for contacts who have not sent you a shard
- [x] Improve 'Lost password' dialog.
- [x] When setting up a backup, if you have less than two contacts, do not display the threshold selector screen as it is impossible to choose enough contacts
- [x] Generally improve explanations on explainer screens
- [x] Improve visualisation of threshold settings
- [ ] Improve screen showing existing social backup, and add delivery confirmation of backup pieces
- [ ] Add an extra warning when deleting a contact who is a custodian
- Notification on backup updates - ***Decided against this***
- Initial explainer screen on setup before choosing custodians - ***Decided against this***
- Notification/visualisation when recovering shards held for others - ***Decided against this***

### Remote wipe 

#### Back end

- [x] Make it possible to revoke wiper status - sending and processing 'revoke' messages
- Change wipe message expiration time

#### UI

- [x] User interface for revoking wiper status
- [x] Change set of trusted contacts
- [x] Fix bug when affirming revoke wipe activated
- [x] Add explainer screen during setup
- [x] Improve explainer screen during wipe activation - now displays the contact's name to avoid activating for incorrect contact.
- Combine with social backup, having one trusted support group for both features - ***Out of scope for this round***

### Protocol / documentation changes

- [x] 2.4.1 in report - Separate shard messages from encrypted backup messages
- [x] 2.4.5 in report - Mutual dependency by backing up shard messages held for others 

# Protocol changes from [security review report](https://darkcrystal.pw/assets/2021_Q3_OTF_and_Magma_Collective_-_Dark_Crystal_Protocol_-_Report.pdf)

## Low-risk findings

### L1: Out-of-Date Library in Use

- [x] Switch spongy castle to bouncy castle

## Informational findings

### I1: Malicious Custodian Can Disrupt Recovery

- [x] Shard verification at high level, and/or more descriptive error message on unverified shard

### I2: Resource Exhaustion Under No/Weak Consent Custodian Model

- [x] Update threat model / protocol specification to cover this issue
- [x] Limit message size in `dark-crystal-key-backup-java`

### I3: Lack of Validation in Share Size May Allow Buffer Overrun

- [x] Update `dark-crystal-shamir-secret-sharing` to throw security exception on invalid secret size

