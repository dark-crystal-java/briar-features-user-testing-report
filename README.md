
# Dark Crystal - Briar Case Project Report

- [Report](https://gitlab.com/dark-crystal-java/briar-features-user-testing-report/-/raw/master/report.pdf)
- [build script](./)
- [markdown source](./report.md)
- [Improvements tracking document](./improvements.md)
